import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField,
  PropertyPaneLink,
  PropertyPaneLabel
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { escape } from '@microsoft/sp-lodash-subset';
import * as jquery from 'jquery';
import styles from './MessageWebPart.module.scss';
import * as strings from 'MessageWebPartStrings';
import {
  SPHttpClient,
  SPHttpClientResponse
} from '@microsoft/sp-http';
// const director :any = require('./assets/Scott.png');
// const background :any = require('./assets/Cummins.jpg');
// const quotes :any = require('./assets/Quotes.png');
export interface IMessageWebPartProps {
  Message: string;
  ListURL: string;
  UserWWID:string;
}
export interface ISPLists {
  value: ISPList[];
}
export interface ISPList {
Title: string;
Designation: string;
Message :string;
Profile:string;
UserName:string;
UserTitle:string;
UserMessage:string;
UserImg:string;
UserEmail:string;
}
export default class MessageWebPart extends BaseClientSideWebPart<IMessageWebPartProps> {
private UserData:ISPList={UserName:"",
                          UserTitle:"",
                          UserMessage:"",
                          UserImg:"",
                          UserEmail:"",Title:"",Designation:"",Message:"",Profile:""};

  public render(): void {
    this.domElement.innerHTML = `
      <div class="${ styles.message }">
        <div class="${styles.messagecardWrap}">
          <div class="${styles.messagecard}" id="spListContainer" style="--accent-color:#a951ed;--accent-width:3px">
          </div>
        </div>    
      </div>`;
      this._renderListAsync();
  }
  // public render(): void {
  //   this.domElement.innerHTML = `
  //     <div class="${ styles.message }">
  //       <div class="${styles.messagecardWrap}">
  //         <div class="${styles.messagecard}" style="--accent-color:#a951ed;--accent-width:3px">
  //            <div style="width:100%;"><img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/Message/Background.png" alt="director" class="${styles.messageHeaderImg}">
  //            </div>
  //            <img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/Message/Scott.jpg" alt="Sundar" class="${styles.messagecardImage}">
  //            <h3 class="${styles.messagecardName}">Scott Nabors</h3>
  //            <p class="${styles.messagecardQuote}">Executive Director</p>
  //            <div class="${styles.messagecardCompanyInfo}">
  //              <p>Helping Cummins deliver best in class IT solutions.Stay up to speed on the latest news from Cummins by signing up for the Cummins Technology newsletter.</p>
  //              <img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/Message/Quotes.png" alt="quotes" class="${styles.quotes}">
  //            </div>
  //         
  //         </div>
  //       </div>    
  //     </div>`;
  //  
  // }
  private _renderListAsync(): void{
    // this.getListData()
    //     .then((response) => {
    //       this._renderList(response.value);
    //     });

    this.getUserData();
    this._renderListUser(this.UserData);
  }
  private getListData():Promise<ISPLists>
  {
    return this.context.spHttpClient.get(this.context.pageContext.web.absoluteUrl+"/_api/web/lists/GetByTitle('Director%20Message')/Items",SPHttpClient.configurations.v1)
    .then((response:SPHttpClientResponse)=>{
      return response.json();
    });
  }
  private _renderList(items: ISPList[]): void 
   {
    let html: string = '';
    items.forEach((item: ISPList) => {
      let msg = item.Message.length>260?item.Message.substr(0,260)+"...":item.Message;
      html += `
      <div style="width:100%;">
      <img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/Message/Background.png" alt="director" class="${styles.messageHeaderImg}">
      </div>
          <img src="${item.Profile.match('"serverRelativeUrl":(.*),"id"')[1].replace(/['"]+/g, '')}" alt="director" class="${styles.messagecardImage}"/>
          <h3 class="${styles.messagecardName}">${item.Title}</h3>
          <p class="${styles.messagecardQuote}">${item.Designation}</p>
          <div class="${styles.messagecardCompanyInfo}">
              <p>${msg}</p>
              <img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/Message/Quotes.png" alt="quotes" class="${styles.quotes}">
            </div>
          `;

    });
  //html += '</div>'
    const listContainer: Element = this.domElement.querySelector('#spListContainer');
    listContainer.innerHTML = html;
  }
  private getUserData()
  {
    let User:any[]=[];
    //console.log("getListData");
    let wwid= this.properties.UserWWID;
    let userUrl = `${this.context.pageContext.web.absoluteUrl}/_api/SP.UserProfiles.PeopleManager/GetPropertiesFor(accountName='i:0%23.f|membership|${wwid}@cummins.com')?$select=DisplayName,Title,Email`;
    //console.log("userUrl"+userUrl);
    jquery.ajax({    
      url: userUrl,    
      type: "GET",    
      headers:{'Accept': 'application/json; odata=verbose;'},  
      async:false,  
      success: function(resultData) {   
        //onsole.log(resultData.d.DisplayName);
        User.push(resultData.d);
        // User.UserName=resultData.d.DisplayName;
        // User.UserTitle = resultData.d.Title;
        // User.UserEmail = resultData.d.Email;
        // User.UserImg = "https://cummins365.sharepoint.com/_layouts/15/userphoto.aspx?size=M&username="+resultData.d.Email;
      },    
      error : function(jqXHR, textStatus, errorThrown) {   
        console.log("componentDidMount error"); 
      }    
  });
    //console.log("User:" + User[0]);
    if(User.length>0)
    { 
    this.UserData.UserName =User[0].DisplayName;
    this.UserData.UserTitle = User[0].Title;
    this.UserData.UserEmail = User[0].Email;
    this.UserData.UserMessage =this.properties.Message;
    this.UserData.UserImg = "https://cummins365.sharepoint.com/_layouts/15/userphoto.aspx?size=L&username="+User[0].Email;
    //console.log(this.UserData);
    }
  }
  private _renderListUser(item: ISPList): void 
  {
   let html: string = '';
     let msg = this.properties.Message==null?"":this.properties.Message.length>260?this.properties.Message.substr(0,260)+"...":this.properties.Message;
     html += `
     <div style="width:100%;">
     <img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/Message/Background.png" alt="director" class="${styles.messageHeaderImg}">
     </div>
         <img src="${item.UserImg}" alt="director" class="${styles.messagecardImage}"/>
         <h3 class="${styles.messagecardName}">${item.UserName}</h3>
         <p class="${styles.messagecardQuote}">${item.UserTitle}</p>
         <div class="${styles.messagecardCompanyInfo}">
             <p>${msg}</p>
             <img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/Message/Quotes.png" alt="quotes" class="${styles.quotes}">
           </div>
         `;
 //html += '</div>'
   const listContainer: Element = this.domElement.querySelector('#spListContainer');
   listContainer.innerHTML = html;
 }
  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }
  private validateWWID(value: string): string {
    if (value === null ||
      value.trim().length === 0) {
      return 'Please enter WWID';
    }
    return '';
  }
  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('UserWWID', {
                  label: strings.WWIDFieldLabel,
                  onGetErrorMessage:this.validateWWID.bind(this)
                }),
                PropertyPaneTextField('Message', {
                  label: strings.MessageFieldLabel,
                  multiline:true
                })                
                  // PropertyPaneLink('ListURL',{
                //   text:'Click here to configure this webpart',href:'https://cummins365.sharepoint.com/sites/CS45455_dev/Lists/Director%20Message/DispForm.aspx?ID=1',target:'_blank'
                // }),
              ]
            }
          ]
        }
      ]
    };
  }
}
