declare interface IMessageWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  MessageFieldLabel: string;
  WWIDFieldLabel:string;
}

declare module 'MessageWebPartStrings' {
  const strings: IMessageWebPartStrings;
  export = strings;
}
